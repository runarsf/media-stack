#!/bin/sh
set +o errexit

if test -z "${HEALTHCHECK_HOST_IP}"; then
  printf "HEALTHCHECK_HOST_IP not set, aborting...\n"
  exit 0
fi

IP="$(dig +short myip.opendns.com @resolver1.opendns.com)"

if test "${?}" -ne "0" -o -z "${IP}"; then
  printf "Something went wrong when fetching public IP, aborting...\nHEALTHCHECK_HOST_IP: ${HEALTHCHECK_HOST_IP}\nIP: ${IP}\n"
  exit 0
fi

if test "${IP}" = "${HOST_IP}"; then
  printf "Warning! Host IP is the same as machine IP...\n"
  exit 1
else
  exit 0
fi
